<?php
/* uTemplate
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace Mingos\uTemplate;

use Mingos\uTemplate\Engine;

class EngineTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var Engine
	 */
	private $engine;

	/**
	 * Test view scripts directory
	 * @var string
	 */
	private $dir;

	public function setUp()
	{
		$this->engine = new Engine();
		$this->dir = __DIR__ . "/resources/uTemplate/EngineTest/";
	}

	/**
	 * It should be possible to add variables one by one and in a single method call.
	 */
	public function testVarCreation()
	{
		// add variables using both provided methods
		$this->engine->addVar("foo", 1);
		$this->engine->addVars(array("bar" => 2, "baz" => 3));

		// create a reflection class to retrieve the private member $vars
		$reflection = new \ReflectionClass($this->engine);
		$vars = $reflection->getProperty("vars");
		$this->assertTrue($vars->isPrivate());
		$vars->setAccessible(true);
		$vars = $vars->getValue($this->engine);

		// assertions
		$this->assertTrue(is_array($vars));
		$this->assertCount(3, $vars);
		$this->assertArrayHasKey("foo", $vars);
		$this->assertArrayHasKey("bar", $vars);
		$this->assertArrayHasKey("baz", $vars);
		$this->assertEquals(1, $vars["foo"]);
		$this->assertEquals(2, $vars["bar"]);
		$this->assertEquals(3, $vars["baz"]);
	}

	/**
	 * Variables should be retrievable one by one and as the entire array
	 */
	public function testVarRetrieval()
	{
		$this->engine->addVar("foo", 1);
		$this->engine->addVars(array("bar" => 2, "baz" => 3));

		// get all variables
		$vars = $this->engine->getVars();

		// assertions
		$this->assertTrue(is_array($vars));
		$this->assertCount(3, $vars);
		$this->assertArrayHasKey("foo", $vars);
		$this->assertArrayHasKey("bar", $vars);
		$this->assertArrayHasKey("baz", $vars);
		$this->assertEquals(1, $vars["foo"]);
		$this->assertEquals(2, $vars["bar"]);
		$this->assertEquals(3, $vars["baz"]);

		// get a single variable
		$var = $this->engine->getVar("foo");
		$this->assertEquals(1, $var);

		$var = $this->engine->getVar("bar");
		$this->assertEquals(2, $var);

		$var = $this->engine->getVar("baz");
		$this->assertEquals(3, $var);
	}

	/**
	 * When requesting a variable that does not exist, an exception must be thrown
	 */
	public function testInvalidVarRetrieval()
	{
		$this->setExpectedException("\\Mingos\\uTemplate\\VariableNotFoundException", "Variable \"\$foo\" not found.", 500);

		$this->assertCount(0, $this->engine->getVars());

		// let's get the party started!
		$this->engine->getVar("foo");
	}

	/**
	 * It should be possible to add view scripts one by one and in a single method call.
	 */
	public function testViewScriptCreation()
	{
		// add view scripts using both provided methods
		$this->engine->addViewScript("foo", 1);
		$this->engine->addViewScripts(array("bar" => 2, "baz" => 3));

		// create a reflection class to retrieve the private member $vars
		$reflection = new \ReflectionClass($this->engine);
		$viewscripts = $reflection->getProperty("viewScripts");
		$this->assertTrue($viewscripts->isPrivate());
		$viewscripts->setAccessible(true);
		$viewscripts = $viewscripts->getValue($this->engine);

		// assertions
		$this->assertTrue(is_array($viewscripts));
		$this->assertCount(3, $viewscripts);
		$this->assertArrayHasKey("foo", $viewscripts);
		$this->assertArrayHasKey("bar", $viewscripts);
		$this->assertArrayHasKey("baz", $viewscripts);
		$this->assertEquals(1, $viewscripts["foo"]);
		$this->assertEquals(2, $viewscripts["bar"]);
		$this->assertEquals(3, $viewscripts["baz"]);
	}

	/**
	 * View scripts should be retrievable one by one and as the entire array
	 */
	public function testViewScriptRetrieval()
	{
		$this->engine->addViewScript("foo", 1);
		$this->engine->addViewScripts(array("bar" => 2, "baz" => 3));

		// get all view scripts
		$viewScripts = $this->engine->getViewScripts();

		// assertions
		$this->assertTrue(is_array($viewScripts));
		$this->assertCount(3, $viewScripts);
		$this->assertArrayHasKey("foo", $viewScripts);
		$this->assertArrayHasKey("bar", $viewScripts);
		$this->assertArrayHasKey("baz", $viewScripts);
		$this->assertEquals(1, $viewScripts["foo"]);
		$this->assertEquals(2, $viewScripts["bar"]);
		$this->assertEquals(3, $viewScripts["baz"]);

		// get a single view script
		$viewScript = $this->engine->getViewScript("foo");
		$this->assertEquals(1, $viewScript);

		$viewScript = $this->engine->getViewScript("bar");
		$this->assertEquals(2, $viewScript);

		$viewScript = $this->engine->getViewScript("baz");
		$this->assertEquals(3, $viewScript);
	}

	/**
	 * When requesting a view script that does not exist, an exception must be thrown
	 */
	public function testInvalidViewScriptRetrieval()
	{
		$this->setExpectedException("\\Mingos\\uTemplate\\ViewScriptNotFoundException", "View script \"\$foo\" not found.", 500);

		$this->assertCount(0, $this->engine->getViewScripts());

		// let's get the party started!
		$this->engine->getViewScript("foo");
	}

	/**
	 * It should be possible to add filters one by one and in a single method call.
	 */
	public function testFilterCreation()
	{
		// add view scripts using both provided methods
		$this->engine->addFilter("foo", 1);
		$this->engine->addFilters(array("bar" => 2, "baz" => 3));

		// create a reflection class to retrieve the private member $vars
		$reflection = new \ReflectionClass($this->engine);
		$viewscripts = $reflection->getProperty("filters");
		$this->assertTrue($viewscripts->isPrivate());
		$viewscripts->setAccessible(true);
		$viewscripts = $viewscripts->getValue($this->engine);

		// assertions
		$this->assertTrue(is_array($viewscripts));
		$this->assertCount(3, $viewscripts);
		$this->assertArrayHasKey("foo", $viewscripts);
		$this->assertArrayHasKey("bar", $viewscripts);
		$this->assertArrayHasKey("baz", $viewscripts);
		$this->assertEquals(1, $viewscripts["foo"]);
		$this->assertEquals(2, $viewscripts["bar"]);
		$this->assertEquals(3, $viewscripts["baz"]);
	}

	/**
	 * View scripts should be retrievable one by one and as the entire array
	 */
	public function testFilterRetrieval()
	{
		$this->engine->addFilter("foo", 1);
		$this->engine->addFilters(array("bar" => 2, "baz" => 3));

		// get all filters
		$filters = $this->engine->getFilters();

		// assertions
		$this->assertTrue(is_array($filters));
		$this->assertCount(3, $filters);
		$this->assertArrayHasKey("foo", $filters);
		$this->assertArrayHasKey("bar", $filters);
		$this->assertArrayHasKey("baz", $filters);
		$this->assertEquals(1, $filters["foo"]);
		$this->assertEquals(2, $filters["bar"]);
		$this->assertEquals(3, $filters["baz"]);

		// get a single filter
		$filter = $this->engine->getFilter("foo");
		$this->assertEquals(1, $filter);

		$filter = $this->engine->getFilter("bar");
		$this->assertEquals(2, $filter);

		$filter = $this->engine->getFilter("baz");
		$this->assertEquals(3, $filter);
	}

	/**
	 * When requesting a filter that does not exist, an exception must be thrown
	 */
	public function testInvalidFilterRetrieval()
	{
		$this->setExpectedException("\\Mingos\\uTemplate\\FilterNotFoundException", "Filter \"foo\" not found.", 500);

		$this->assertCount(0, $this->engine->getFilters());

		// let's get the party started!
		$this->engine->getFilter("foo");
	}

	/**
	 * Integration test using the Engine & the View objects together. Rendering must be correct.
	 */
	public function testRendering()
	{
		$this->engine->addVar("person", "lumberjack");
		$this->engine->addViewScript("lumberjack", $this->dir . "testRendering.phtml");

		$output = $this->engine->render("lumberjack");

		$this->assertEquals("<p>I'm a lumberjack and I'm OK.</p>\n", $output);
	}

	/**
	 * Check if the view scripts directory setting is correct.
	 */
	public function testViewScriptsDir()
	{
		$this->engine->setViewScriptDir("/lumberjack/");
		$this->engine->addViewScript("lumberjack", "cut/down/trees.phtml");

		$this->assertEquals("/lumberjack/cut/down/trees.phtml", $this->engine->getViewScript("lumberjack"));
	}
}
