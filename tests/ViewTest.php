<?php
/* uTemplate
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace Mingos\uTemplate;

use Mingos\uTemplate\View;
use Mockery as m;

class ViewTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var m\MockInterface
	 */
	private $engine;

	/**
	 * Test view scripts directory
	 * @var string
	 */
	private $dir;

	/**
	 * Set the test suite up
	 */
	public function setUp()
	{
		$this->engine = m::mock("\\Mingos\\uTemplate\\Engine");
		$this->dir = __DIR__ . "/resources/uTemplate/ViewTest/";
	}

	/**
	 * Test whether rendering a view actually renders the contents of the view script file.
	 */
	public function testViewRendering()
	{
		$view = new View($this->engine, $this->dir . "testViewRendering.phtml");
		$output = $view->render();

		$this->assertEquals("<h1>Hello, world!</h1>\n", $output);
	}

	/**
	 * If trying to render a view using a nonexistent view script, an exception must be thrown.
	 */
	public function testViewRenderingNonexistentFile()
	{
		$this->setExpectedException("\\Mingos\\uTemplate\\FileNotFoundException", "File iDontExist.phtml not found.", 500);

		$view = new View($this->engine, "iDontExist.phtml");
		$view->render();
	}

	/**
	 * A view must be able to render another view.
	 */
	public function testNestedViewRendering()
	{
		$this->engine->shouldReceive("getViewScripts")->once()
			->andReturn(array("nested" => $this->dir . "testNestedViewRendering2.phtml"));

		$view = new View($this->engine, $this->dir . "testNestedViewRendering.phtml");
		$output = $view->render();

		$this->assertEquals("<h1>Lumberjack Song</h1>\n<p>I'm a lumberjack and I'm OK,</p>\n<p>I sleep all night and I work all day.</p>\n", $output);
	}

	/**
	 * If trying to recursively render a view (rendering self within self), an exception should be thrown.
	 */
	public function testNestedViewRenderingRecursion()
	{
		$this->setExpectedException("\\Mingos\\uTemplate\\RecursionException", "Attempt to render the view recursively.", 500);

		$view = new View($this->engine, $this->dir . "testNestedViewRenderingRecursion.phtml");
		$view->render();
	}

	/**
	 * A filter may be used if it is a callable.
	 */
	public function testFilterCallable()
	{
		$input = "Hello, world!";

		$this->engine->shouldReceive("getFilter")->with("alnum")->once()->andReturn(function($value) {
			return "Hello world";
		});

		$view = new View($this->engine, "iDontExist.phtml");
		$view->filter("alnum", $input); // no exception
	}

	/**
	 * A filter may be used if it is an instance of a class implementing the method filter().
	 */
	public function testFilterObject()
	{
		$input = "Hello, world!";

		$filter = m::mock("Alnum");
		$filter->shouldReceive("filter")->with($input)->once()->andReturn("Hello world");

		$this->engine->shouldReceive("getFilter")->with("alnum")->once()->andReturn($filter);

		$view = new View($this->engine, "iDontExist.phtml");
		$view->filter("alnum", $input); // no exception
	}

	/**
	 * Non-callables and classes without the filter() method cannot be used to filter content and should
	 * cause an exception to be thrown.
	 */
	public function testFilterInvalid()
	{
		$input = "Hello, world!";

		$this->setExpectedException("\\Mingos\\uTemplate\\InvalidFilterException", "The filter \"alnum\" is not a valid filter.", 500);

		$this->engine->shouldReceive("getFilter")->with("alnum")->once()->andReturn("invalid");

		$view = new View($this->engine, "iDontExist.phtml");
		$view->filter("alnum", $input);
	}

	/**
	 * The escape() function must pass the input through htmlentities().
	 */
	public function testEscape()
	{
		$view = new View($this->engine, "iDontExist.phtml");

		$this->assertEquals(
			"&lt;p&gt;unsafe!&lt;/p&gt;",
			$view->escape("<p>unsafe!</p>")
		);
		$this->assertEquals(
			"&lt;script&gt;alert(&#039;foo&#039;);&lt;/script&gt;",
			$view->escape("<script>alert('foo');</script>")
		);
		$this->assertEquals(
			"&quot;I&#039;m a lumberjack and I&#039;m OK&quot;",
			$view->escape("\"I'm a lumberjack and I'm OK\"")
		);
	}

	/**
	 * The view should be able to successfully render variables extracted from the engine and variables passed in via
	 * the second parametre to the View::render() method. If there is a variable with a name clashing with one of the
	 * class properties of View, it should still be rendered normally via $this->varName.
	 */
	public function testRenderVariables()
	{
		if (defined("HHVM_VERSION")) {
			$this->fail("HHVM not suported at this time; this test will segfault when run on HHVM.");
		}

		$this->engine->shouldReceive("getVar")->with("global")->once()->andReturn("Global var");
		$this->engine->shouldReceive("getVar")->with("engine")->once()->andReturn("Engine var");
		$view = new View($this->engine, $this->dir . "/testRenderVariables.phtml");

		$output = $view->render(null, array("local" => "Local var")); // render(null) segfaults on HHVM

		$this->assertEquals("<p>Global var</p>\n<p>Local var</p>\n<p>Engine var</p>\n", $output);
	}

	/**
	 * Close Mockery
	 */
	public function tearDown()
	{
		m::close();
	}
}
