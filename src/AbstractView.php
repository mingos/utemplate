<?php
/* uTemplate
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace Mingos\uTemplate;

/**
 * Abstract View class.
 *
 * It is used uniquely for the purpose of hiding private class properties from the derived class context.
 *
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 2.0-dev
 */
abstract class AbstractView
{
	/**
	 * Filename of the view script
	 *
	 * @var string
	 * @since 0.1-dev
	 */
	private $viewScript;

	/**
	 * Reference to the parent engine
	 *
	 * @var Engine
	 */
	private $engine;

	/**
	 * A flag telling whether the object is already being rendered.
	 * This is to prevent recursive rendering of the view script.
	 *
	 * @var boolean
	 * @since 1.3-dev
	 */
	private $isBeingRendered = false;

	/**
	 * Array of local variables available in the view via $this->variableName
	 *
	 * @var array
	 * @since 2.0
	 */
	private $variables = array();

	/**
	 * Constructor. Create an instance of a view.
	 *
	 * @param Engine $engine     Reference to the parent Engine object
	 * @param string $viewScript Filename of the view script
	 *
	 * @since 0.1-dev
	 */
	public function __construct(Engine $engine, $viewScript)
	{
		$this->engine = $engine;
		$this->viewScript = $viewScript;
	}

	/**
	 * Check whether the view is currently being rendered
	 *
	 * @return boolean
	 *
	 * @since 2.0
	 */
	protected function isBeingRendered()
	{
		return $this->isBeingRendered;
	}

	/**
	 * Set the isBeingRendered flag of the view
	 *
	 * @param boolean $flag
	 *
	 * @since 2.0
	 */
	protected function setBeingRendered($flag)
	{
		$this->isBeingRendered = (boolean) $flag;
	}

	/**
	 * Set local variables
	 *
	 * @param array $variables Array of local variables
	 *
	 * @since 2.0
	 */
	protected function setVariables($variables)
	{
		$this->variables = $variables;
	}

	/**
	 * Fetch the engine object
	 *
	 * @return Engine The Engine object
	 *
	 * @since 2.0
	 */
	protected function getEngine()
	{
		return $this->engine;
	}

	/**
	 * Fetch a filter from the engine
	 *
	 * @param  string $name Filter name to fetch from the Engine
	 * @return mixed
	 *
	 * @since 2.0
	 */
	protected function getFilter($name)
	{
		return $this->engine->getFilter($name);
	}

	/**
	 * Fetch the view script's filename
	 *
	 * @return string View script filename
	 *
	 * @since 2.0
	 */
	protected function getViewScript()
	{
		return $this->viewScript;
	}

	/**
	 * Access a variable from the engine
	 *
	 * @param  string $name Variable name
	 * @return mixed        Variable
	 *
	 * @since 2.0
	 */
	public function __get($name)
	{
		return array_key_exists($name, $this->variables) ?
			$this->variables[$name] :
			$this->engine->getVar($name);
	}
}
