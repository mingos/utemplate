<?php
/* uTemplate
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace Mingos\uTemplate;

/**
 * The main class for the uTemplate PHP template engine.
 *
 * In order to use it, it's enough to create an instance, add a view script and render it:
 *
 * <code>
 * $engine = new Engine();
 * echo $engine
 *     ->addViewScript("layout", "layout.phtml")
 *     ->render("layout");
 * </code>
 *
 * Since uTemplate will communicate with the developer via exceptions, it's a good idea to enclose all uTemplate-related
 * code in try-cacth blocks.
 *
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.1-dev
 */
class Engine
{
	/**
	 * uTemplate version
	 *
	 * @var string
	 * @since 0.1-dev
	 */
	const VERSION = "2.0.0";

	/**
	 * Array of defined view scripts
	 *
	 * @var array
	 * @since 0.1-dev
	 */
	private $viewScripts = array();

	/**
	 * Array of global variables available in the view scripts
	 *
	 * @var array
	 * @since 0.1-dev
	 */
	private $vars = array();

	/**
	 * Array of filters prepared for use in view scripts
	 *
	 * @var array
	 * @since 2.0
	 */
	private $filters = array();

	/**
	 * The directory where view scripts reside
	 *
	 * @var string
	 * @since 2.0
	 */
	private $viewScriptDir = "";

	/**
	 * Set the root directory of the view scripts.
	 * Once this is set, the view scripts' paths will be relative to this value.
	 *
	 * @param  string $viewScriptDir The root directory of the view scripts
	 * @return Engine
	 *
	 * @since 2.0
	 */
	public function setViewScriptDir($viewScriptDir)
	{
		$this->viewScriptDir = $viewScriptDir;

		return $this;
	}

	/**
	 * Fetch the view scripts root directory.
	 *
	 * @return string
	 *
	 * @since 2.0
	 */
	public function getViewScriptDir()
	{
		return $this->viewScriptDir;
	}

	/**
	 * Add a new view to the template
	 *
	 * The view script is always named, e.g.
	 *
	 * <code>
	 * $engine->addViewScript("content", "/path/to/view/script.phtml");
	 * </code>
	 *
	 * This enables the use of the name inside a view script to render a nested script:
	 *
	 * <code>
	 * <?php echo $this->render("content"); ?>
	 * </code>
	 *
	 * @param  string $name       View script name
	 * @param  string $viewScript View script's filename
	 * @return Engine             Provides a fluent interface
	 *
	 * @see View::render()
	 * @since 0.1-dev
	 */
	public function addViewScript($name, $viewScript)
	{
		$this->viewScripts[$name] = $this->viewScriptDir . $viewScript;

		return $this;
	}

	/**
	 * Add an array of view scripts to the template
	 *
	 * @param  array  $viewScripts Array of name => filename pairs
	 * @return Engine              Provides a fluent interface
	 *
	 * @see Engine::addViewScript()
	 * @since 0.1-dev
	 */
	public function addViewScripts($viewScripts)
	{
		foreach ($viewScripts as $name => $viewScript) {
			$this->viewScripts[$name] = $this->viewScriptDir . $viewScript;
		}

		return $this;
	}

	/**
	 * Fetch all defined view scripts
	 *
	 * @return array Array od defined view scripts
	 *
	 * @since 0.1-dev
	 */
	public function getViewScripts()
	{
		return $this->viewScripts;
	}

	/**
	 * Fetch a single view script
	 *
	 * @param  string                      $name View script name
	 * @return string                            View script filename
	 * @throws ViewScriptNotFoundException
	 *
	 * @since 2.0
	 */
	public function getViewScript($name)
	{
		if (array_key_exists($name, $this->viewScripts)) {
			return $this->viewScripts[$name];
		}

		throw new ViewScriptNotFoundException("View script \"\${$name}\" not found.", 500);
	}

	/**
	 * Add a new variable to the view scripts<br>
	 *
	 * The variable requires a name for it. The view script's rendering method will take all variables defined this way
	 * in the Engine object and make them available in the view script. All view scripts will have direct access to the
	 * defined variables via the $this keyword. For instance, if a variable is added in the Engine class:
	 *
	 * <code>
	 * $engine->addVar("foo", "bar");
	 * </code>
	 *
	 * then all view scripts may use the variable locally:
	 *
	 * <code>
	 * <?php echo $this->foo; // outputs "bar" ?>
	 * </code>
	 *
	 * @param  string $name  Variable name
	 * @param  mixed  $value Variable value
	 * @return Engine        Provides a fluent interface
	 *
	 * @since 0.1-dev
	 */
	public function addVar($name, $value)
	{
		$this->vars[$name] = $value;

		return $this;
	}

	/**
	 * Add an array of variables to the view scripts
	 *
	 * @param  array  $vars Array of name-value pairs
	 * @return Engine       Provides a fluent interface
	 *
	 * @see Engine::addVar()
	 * @since 0.1-dev
	 */
	public function addVars($vars)
	{
		foreach ($vars as $name => $value) {
			$this->vars[$name] = $value;
		}

		return $this;
	}

	/**
	 * Fetch all defined variables as a keyed array.
	 *
	 * @return array Array of global variables
	 *
	 * @since 0.1-dev
	 */
	public function getVars()
	{
		return $this->vars;
	}

	/**
	 * Fetch a variable
	 *
	 * @param  string                    $name Variable name
	 * @return mixed                           Requested variable
	 * @throws VariableNotFoundException       Thrown when the requested variable does not exist
	 */
	public function getVar($name)
	{
		if (array_key_exists($name, $this->vars)) {
			return $this->vars[$name];
		}

		throw new VariableNotFoundException("Variable \"\${$name}\" not found.", 500);
	}

	/**
	 * Define a single filter
	 *
	 * @param  string $name   Filter name
	 * @param  mixed  $filter Either a callable (e.g. a closure) or an object that implements a publicly accessible
	 *                        filter() method
	 * @return Engine         Provides a fluent interface
	 *
	 * @since 2.0
	 */
	public function addFilter($name, $filter)
	{
		$this->filters[$name] = $filter;

		return $this;
	}

	/**
	 * Define multiple filters in a single function call
	 *
	 * @param  array  $filters Array of $name => $filter pairs
	 * @return Engine          Provides a fluent interface
	 *
	 * @since 2.0
	 */
	public function addFilters($filters)
	{
		foreach ($filters as $name => $filter) {
			$this->filters[$name] = $filter;
		}

		return $this;
	}

	/**
	 * Fetch all defined filters as a keyed array.
	 *
	 * @return array Array of filters
	 *
	 * @since 2.0
	 */
	public function getFilters()
	{
		return $this->filters;
	}

	/**
	 * Fetch a named filter
	 *
	 * @param  string                  $name Filter name
	 * @return mixed                         The filter
	 * @throws FilterNotFoundException       Thrown when the requested filter is not found
	 *
	 * @since 2.0
	 */
	public function getFilter($name)
	{
		if (array_key_exists($name, $this->filters)) {
			return $this->filters[$name];
		}

		throw new FilterNotFoundException("Filter \"{$name}\" not found.", 500);
	}

	/**
	 * Render a view script.
	 *
	 * This is typically the very last thing called inside the application if used to render the full template.
	 *
	 * @param  null|string $name      The template name to render
	 * @param  array       $variables Additional local variables passed to the view script
	 * @return string
	 *
	 * @since 0.1-dev
	 */
	public function render($name, $variables = array())
	{
		if (array_key_exists($name, $this->viewScripts)) {
			$name = $this->viewScripts[$name];
		} else {
			$name = $this->viewScriptDir . $name;
		}

		$view = new View($this, $name);

		return $view->render(null, $variables);
	}
}
