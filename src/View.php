<?php
/* uTemplate
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace Mingos\uTemplate;

/**
 * View class.
 *
 * The view class is not meant to be instantiated by the user. It will be used internally by the Engine class to
 * produce the output of the rendered view script.
 *
 * While inside a view script, the variables and methods are available via the $this keyword.
 *
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.1-dev
 */
class View extends AbstractView
{
	/**
	 * Render a view
	 *
	 * When inside the view script, this method will be available via the $this keyword. To render a nested view script,
	 * it's enough to invoke this code in the script:
	 *
	 * <code>
	 * <?php echo $this->render("scriptName"); ?>
	 * </code>
	 *
	 * Where "scriptName" is the name of a view script defined inside the template class using the
	 * Engine::varAdd() method. Alternatively, it is possible to render any view script by simply specifying its full
	 * filename with path:
	 *
	 * <code>
	 * <?php echo $this->render("/path/to/view/script.phtml"); ?>
	 * </code>
	 *
	 * The script rendered this way will be able to use the same resources (variables) as any other view script.
	 *
	 * The method may also be used with no arguments, in which case it will not render a nested view script, but rather
	 * the current one. It should never be used inside the view script, as it will result in an infinite loop!
	 *
	 * @param  string $name          Either the name of a previously defined view or the filename of the view script to
	 *                               render
	 * @param  array $variables      Additional variables that will be passed to the view script. They may substitute
	 *                               some variables from the Engine object, but the changes are local and won't carry
	 *                               over to other view scripts.
	 * @return string                Rendered view
	 * @throws FileNotFoundException Thrown if the provided view script file name does not exist
	 * @throws RecursionException    Thrown if recursive rendering is detected
	 *
	 * @see Engine::viewScriptAdd()
	 * @see Engine::varAdd()
	 * @since 0.1-dev
	 */
	public function render($name = null, $variables = array())
	{
		// set variables
		$this->setVariables($variables);

		if (null === $name) { // render self
			$view = $this->getViewScript();
			if (!is_file($this->getViewScript())) {
				throw new FileNotFoundException("File {$view} not found.", 500);
			}

			if ($this->isBeingRendered()) {
				throw new RecursionException("Attempt to render the view recursively.", 500);
			}

			$this->setBeingRendered(true);

			unset($name);
			unset($variables);

			ob_start();
			include $view;
			$result = ob_get_clean();

			$this->setBeingRendered(false);

			return $result;
		} else { // render a nested view script
			$views = $this->getEngine()->getViewScripts();
			if (array_key_exists($name, $views)) {
				$name = $views[$name];
			} else {
				$name = $this->getEngine()->getViewScriptDir() . $name;
			}

			$view = new View($this->getEngine(), $name);

			return $view->render(null, $variables);
		}
	}

	/**
	 * Pass a value through a filter
	 *
	 * @param  string                 $name  Name of the requested filter
	 * @param  string                 $value The value to filter
	 * @return string                        Filtered string
	 * @throws InvalidFilterException        Thrown if the filter doesn't exist or is invalid
	 *
	 * @since 2.0
	 */
	public function filter($name, $value)
	{
		$filter = $this->getFilter($name);

		if (is_callable($filter)) {
			return $filter($value);
		} else if (is_object($filter) && is_callable(array($filter, "filter"))) {
			return $filter->filter($value);
		}

		throw new InvalidFilterException("The filter \"{$name}\" is not a valid filter.", 500);
	}

	/**
	 * Escape a value
	 *
	 * @param  string  $value Input value
	 * @param  integer $flags Flags to be passed to htmlentities
	 * @return string         Escaped value
	 *
	 * @since 2.0
	 */
	public function escape($value, $flags = ENT_QUOTES)
	{
		return htmlentities($value, $flags);
	}
}
